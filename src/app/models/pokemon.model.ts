export interface Pokemon {
  name: string,
  url: string,
  pokedex: number,
  localIndex: number,
  image: string,
  collected: boolean
}