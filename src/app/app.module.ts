import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeadingComponent } from './components/heading/heading.component';
import { LoginComponent } from './components/login/login.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { CatalogueItemComponent } from './components/catalogue-item/catalogue-item.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TrainerItemComponent } from './components/trainer-item/trainer-item.component';
import { PokemonDetailsComponent } from './components/pokemon-details/pokemon-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadingComponent,
    LoginComponent,
    TrainerComponent,
    CatalogueComponent,
    CatalogueItemComponent,
    PageNotFoundComponent,
    TrainerItemComponent,
    PokemonDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
