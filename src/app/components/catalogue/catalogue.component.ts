import { Component, OnInit } from '@angular/core';

import { CatalogueService } from 'src/app/services/catalogue.service';
import { Pokemon } from '../../models/pokemon.model';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit  {
  pageIndex: number = 0;
  pageSize: number = 25;
  filterTerm: string = '';
  filteredCatalog: Pokemon[] = [];

  constructor(private readonly catalogueService: CatalogueService) { }

  ngOnInit(): void {
    if (this.catalogueService.catalogue.length === 0) {
      this.catalogueService.fetchCatalogue();
    }
  }

  get catalogue(): Pokemon[] {
    return this.catalogueService.catalogue;
  }

  handleClick(index: number) {
    this.catalogueService.toggleCollected(index);
  }

  get page(): Pokemon[] {
    if (this.filterTerm === '')
      return this.catalogue.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
    else
      return this.filteredCatalog.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  setFilter(value: string) {
    this.filterTerm = value.toLowerCase();
    this.filteredCatalog = this.catalogue.filter(pokemon => pokemon.name.startsWith(this.filterTerm));
    this.pageIndex = 0;
  }

  goBack(): void {
    if (this.pageIndex > 0)
      this.pageIndex--;
  }

  goForward(): void {
    if (this.filterTerm === '') {
      if (this.pageIndex * this.pageSize + this.pageSize < this.catalogue.length)
        this.pageIndex++;
    }
    else {
      if (this.pageIndex * this.pageSize + this.pageSize < this.filteredCatalog.length)
        this.pageIndex++;
    }
  }
}
