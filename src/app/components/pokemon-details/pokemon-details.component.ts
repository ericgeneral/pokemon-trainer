import { Component, OnInit } from '@angular/core';
import { PokemonDetails } from 'src/app/models/pokemonDetails.models';
import { DetailsService } from 'src/app/services/details.service';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.css']
})
export class PokemonDetailsComponent implements OnInit {

  constructor(private readonly detailsService: DetailsService) { }

  ngOnInit(): void {
  }

  get pokemonDetails(): PokemonDetails {
    return this.detailsService.pokemonDetails;
  }

}
