import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ])
  });

  constructor(private readonly formBuilder: FormBuilder,
    private readonly userService: UserService,
    private readonly router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    if (!this.username || this.username.errors) {
      return;
    }
    this.userService.login(this.username.value);
    this.router.navigate(['/catalogue']);
  }

  get username() { return this.loginForm.get('username'); }
}
