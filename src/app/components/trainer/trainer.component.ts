import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { DetailsService } from 'src/app/services/details.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {
  pageIndex: number = 0;
  pageSize: number = 25;
  filterTerm: string = '';
  filteredInventory: Pokemon[] = [];

  constructor(private readonly catalogueService: CatalogueService,
    private readonly detailsService: DetailsService) { }

  ngOnInit(): void {
    if (this.catalogueService.catalogue.length === 0) {
      this.catalogueService.fetchCatalogue();
    }
  }

  handleClick(url: string) {
    this.detailsService.fetchDetails(url);
  }

  get inventory(): Pokemon[] {
    return this.catalogueService.catalogue.filter(pokemon => pokemon.collected);
  }

  get page(): Pokemon[] {
    if (this.filterTerm === '')
      return this.inventory.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
    else
      return this.filteredInventory.slice(this.pageIndex * this.pageSize, this.pageIndex * this.pageSize + this.pageSize);
  }

  setFilter(value: string) {
    this.filterTerm = value.toLowerCase();
    this.filteredInventory = this.inventory.filter(pokemon => pokemon.name.startsWith(this.filterTerm));
    this.pageIndex = 0;
  }

  goBack(): void {
    if (this.pageIndex > 0)
      this.pageIndex--;
  }

  goForward(): void {
    if (this.filterTerm === '') {
      if (this.pageIndex * this.pageSize + this.pageSize < this.inventory.length)
        this.pageIndex++;
    }
    else {
      if (this.pageIndex * this.pageSize + this.pageSize < this.filteredInventory.length)
        this.pageIndex++;
    }
  }
}
