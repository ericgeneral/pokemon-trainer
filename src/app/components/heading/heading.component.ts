import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.css']
})
export class HeadingComponent implements OnInit {
  constructor(private readonly userService: UserService,
    private readonly router: Router) { }

  ngOnInit(): void {
  }

  get username() {
    return this.userService.username;
  }

  onCatalogueClicked() {
    this.router.navigate(['/catalogue']);
  }

  onTrainerClicked() {
    this.router.navigate(['/trainer']);
  }

  onLogoutClicked() {
    this.userService.logout();
    this.router.navigate(['/login']);
  }
}
