import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CatalogueComponent } from "./components/catalogue/catalogue.component";
import { LoginComponent } from "./components/login/login.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { TrainerComponent } from "./components/trainer/trainer.component";
import { LoggedInGuard } from "./guards/logged-in.guard";
import { LoggedOutGuard } from "./guards/logged-out.guard";

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [LoggedOutGuard] },
  { path: 'trainer', component: TrainerComponent, canActivate: [LoggedInGuard] },
  { path: 'catalogue', component: CatalogueComponent, canActivate: [LoggedInGuard] },
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }