import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';

interface CatalogueResponse {
  count: number,
  next: string,
  previous: string,
  results: { 
    name: string,
    url: string
  }[]
}

const localStorageCatalogue = 'catalogue';

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {
  private _catalogue: Pokemon[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient) { }

  fetchCatalogue(): void {
    const catalogFromStorage = localStorage.getItem(localStorageCatalogue);
    if (catalogFromStorage != null) {
      this._catalogue = JSON.parse(catalogFromStorage);
    } else {
      this.http.get<CatalogueResponse>(environment.pokemonAPI + '/?limit=2000') 
        .subscribe(
          (response: CatalogueResponse) => {
            let localIndex = 0;
            response.results.forEach(result => {
              const pokedex = this.getPokedex(result.url);
              const newPokemon: Pokemon = {
                name: result.name,
                url: result.url,
                pokedex: pokedex,
                localIndex: localIndex++,
                image: this.getImageLocation(pokedex),
                collected: false
              };
              this._catalogue.push(newPokemon);
            });
            localStorage.setItem(localStorageCatalogue, JSON.stringify(this._catalogue));
          },
          (error: HttpErrorResponse) => {
            this._error = error.message;
          }
        );
    }
  }

  private getPokedex(pokemonApiUrl: string): number {
    const urlParts = pokemonApiUrl.split('/');
    return parseInt(urlParts[urlParts.length - 2]);
  }

  private getImageLocation(index: number): string {
    return "assets/images/pokemon/" + index + ".png";
  }

  get catalogue(): Pokemon[] {
    return this._catalogue;
  }

  get error(): string {
    return this._error;
  }

  toggleCollected(index: number): void {
    this._catalogue[index].collected = !(this._catalogue[index].collected);
    localStorage.setItem(localStorageCatalogue, JSON.stringify(this._catalogue));
  }

  clearCatalogue(): void {
    this._catalogue = [];
    localStorage.removeItem(localStorageCatalogue);
  }
}
