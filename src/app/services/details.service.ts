import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PokemonDetails } from '../models/pokemonDetails.models';

@Injectable({
  providedIn: 'root'
})
export class DetailsService {
  private _pokemonDetails!: PokemonDetails;
  private _error: string = '';

  constructor(private readonly http: HttpClient) { }

  fetchDetails(url: string): void {
    this.http.get<PokemonDetails>(url) 
    .subscribe(
      (response: PokemonDetails) => {
        this._pokemonDetails = response;
        console.log(this._pokemonDetails);
      },
      (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    );
  }

  get pokemonDetails(): PokemonDetails {
    return this._pokemonDetails;
  }
}
