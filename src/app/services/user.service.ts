import { Injectable } from '@angular/core';
import { CatalogueService } from './catalogue.service';

const localStorageUsername = 'username';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private readonly catalogueService: CatalogueService) { }

  login(username: string): void {
    localStorage.setItem(localStorageUsername, username);
  }

  get username(): string {
    const username = localStorage.getItem(localStorageUsername);
    if (username === null) return '';
    return username;
  }

  logout(): void {
    this.catalogueService.clearCatalogue();
    localStorage.removeItem(localStorageUsername);
  }
}
